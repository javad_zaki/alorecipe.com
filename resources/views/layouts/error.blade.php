<!DOCTYPE html>
<html lang="fa">
    <head>
        <!--Title====================================================================================================-->
        @section('title')@show
        <!--Title====================================================================================================-->

        <!--Global Meta Tags=========================================================================================-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        @section('meta')@show
        <!--Global Meta Tags=========================================================================================-->

        <!--Global Style Assets======================================================================================-->
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('img/favicon/site.webmanifest')}}">

        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
        <link href="{{asset('/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/css/plugins.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/css/pages/error/style-400.css')}}" rel="stylesheet" type="text/css" />
        @section('page_styles')@show
        <!--Global Style Assets======================================================================================-->
    </head>

    @section('body')@show

    <!--Global Scripts===============================================================================================-->
    <script src="{{asset('/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
    @section('page_scripts')@show
    <!--Global Scripts===============================================================================================-->
</html>
