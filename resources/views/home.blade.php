@extends('layouts.app')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('page_styles')
    <link href="{{asset('plugins/editors/quill/quill.snow.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/apps/mailbox.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/sweetalerts/promise-polyfill.js')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/notification/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/components/cards/card.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('body')
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <div class="title">
                            <h3>Recipe Finder</h3>
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"  aria-current="page"><a href="javascript:void(0);">Breakfast, Lunch & Dinner </a></li>
                        </ol>
                    </nav>
                </div>
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="row">
                            <div class="col-xl-12  col-md-12">
                                <div class="mail-box-container">
                                    <div class="mail-overlay"></div>
                                    <div class="tab-title">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-12 text-center mail-btn-container">
                                                <h4>Categories</h4>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-12 mail-categories-container">
                                                <div class="mail-sidebar-scroll">
                                                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions active" id="vegetablesgreens"><span class="nav-names">Vegetables & Greens</span> <span class="mail-badge badge"></span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="important"><span class="nav-names">Herbs & Spices</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="draft"><span class="nav-names">Dairy & Eggs</span> <span class="mail-badge badge"></span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="sentmail"><span class="nav-names">Baking</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="spam"><span class="nav-names">Sugar & Sweeteners</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="trashed"><span class="nav-names">Fruites & Berries</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Oils</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Condiments & Pickles</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Cheeses</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Meats</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Nuts & Seeds</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Bread & Salty Snackes</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Soups & Stocks</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Grains & Cereals</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Beans, Peas & Lentils</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Dairy-free</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Pasta</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Seasonings</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Sauces & Dips</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Fish</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Seafood & Seaweed</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Pre-Made Doughs</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link list-actions" id="oils"><span class="nav-names">Supplements</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="mailbox-inbox" class="accordion mailbox-inbox">
                                        <div class="search">
                                            <input type="text" class="form-control input-search" placeholder="Search for ingredients...">
                                            <button type="button" class="btn btn-sm btn-secondary my-pantries d-none d-lg-block">
                                                My Pantries <span class="badge badge-light">2</span>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-dark delete-all d-none d-lg-block">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                            </button>
                                        </div>
                                        {{--<div class="action-center">

                                        </div>--}}
                                        <div class="message-box">
                                            <div class="message-box-scroll" id="ct">

                                                <div id="unread-onion" class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox" checked>
                                                                        <span class="new-control-indicator"></span>onion
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="unread-onion" class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox" checked>
                                                                        <span class="new-control-indicator"></span>garlic
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>tomato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>bell pepper
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>carrot
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>potato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>scallion
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>ginger
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>red onion
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>celery
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>chili pepper
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>mushroom
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>avacado
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>zucchini
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>corn
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>shallot
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>cherry tomato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>cucumber
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>jalapeno
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>spinach
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>broccoli
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>sweet potato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>cabbage
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>baby greens
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>cauliflower
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>asparagus
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>kale
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>arugula
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>leek
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>green chillies
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>eggplant
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>lettuce
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>pumpkin
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>beetroot
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>butternut squash
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>artichoke heart
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>brussels sprout
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>sugar pumpkin
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>romaine
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>fennel
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>squash
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>radish
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>sun dried tomato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>canned pumpkin
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>horseradish
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>mixed greens
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>shiitake mushroom
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>parsnip
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>bok choy
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>red chillies
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>portobello mushroom
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>snow peas
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>poblano
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>bean sprouts
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>watercress
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>chard
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>iceberg
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>pimiento
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>mixed vegetable
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mail-item vegetablesgreens">
                                                    <div class="mail-item-heading collapsed">
                                                        <div class="mail-item-inner">
                                                            <div class="d-flex">
                                                                <div class="n-chk text-center">
                                                                    <label class="new-control new-checkbox checkbox-success">
                                                                        <input type="checkbox" class="new-control-input inbox-chkbox">
                                                                        <span class="new-control-indicator"></span>green tomato
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="find-center">
                                            <div class="desktop-find-recipe d-none d-lg-block">
                                                <button type="button" class="btn btn-success">
                                                    Find Recipes
                                                </button>
                                            </div>
                                            <div class="mt-2">
                                                <div class="n-chk">
                                                    <label class="new-control new-checkbox checkbox-primary">
                                                        <input type="checkbox" class="new-control-input" id="inboxAll">
                                                        <span class="new-control-indicator"></span><span>Recipes contain only selected ingredients</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="mobile-find-recipe d-lg-none">
                                                <button type="button" class="btn btn-success">
                                                    Find Recipes
                                                </button>
                                                <button type="button" class="btn btn-secondary">
                                                    My Pantries <span class="badge badge-light">2</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-header mt-5">
                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <div class="title">
                            <h3>Recipes</h3>
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"  aria-current="page"><a>You can make <strong class="font-weight-bold text-success">250</strong> recipes</a></li>
                        </ol>
                    </nav>
                </div>
                <div class="row layout-spacing">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4 col-xl-3 mt-5">
                                <div class="card component-card_9">
                                    <img src="{{asset('img/recipes/01.jpg')}}" class="card-img-top" alt="widget-card-2">
                                    <div class="card-body">
                                        <h5 class="card-title">Grilled Sweet Maui Onions</h5>
                                        <p class="card-text">You have all 2 ingredients</p>
                                        <div class="meta-info">
                                            <div class="meta-action">
                                                <button type="button" class="btn btn-block btn-success">
                                                    View Full Recipe
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4 col-xl-3 mt-5">
                                <div class="card component-card_9">
                                    <img src="{{asset('img/recipes/02.jpg')}}" class="card-img-top" alt="widget-card-2">
                                    <div class="card-body">
                                        <h5 class="card-title">Onion-Garlic Puree</h5>
                                        <p class="card-text">You have all 2 ingredients</p>
                                        <div class="meta-info">
                                            <div class="meta-action">
                                                <button type="button" class="btn btn-block btn-success">
                                                    View Full Recipe
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4 col-xl-3 mt-5">
                                <div class="card component-card_9">
                                    <img src="{{asset('img/recipes/03.jpg')}}" class="card-img-top" alt="widget-card-2">
                                    <div class="card-body">
                                        <h5 class="card-title">Easy Homemade Granulated Garlic</h5>
                                        <p class="card-text">You have all 2 ingredients</p>
                                        <div class="meta-info">
                                            <div class="meta-action">
                                                <button type="button" class="btn btn-block btn-success">
                                                    View Full Recipe
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4 col-xl-3 mt-5">
                                <div class="card component-card_9">
                                    <img src="{{asset('img/recipes/04.jpg')}}" class="card-img-top" alt="widget-card-2">
                                    <div class="card-body">
                                        {{--<p class="meta-date">cookstr.com</p>--}}
                                        <h5 class="card-title">Instant Pot Pinto Beans Recipe</h5>
                                        <p class="card-text">
                                            <span class="missing"> You're missing </span>
                                            <span class="attachments">Butter</span>
                                            <span class="attachments">Garlic</span>
                                            <span class="attachments">Egg</span>
                                            <span class="attachments">Tomato</span>
                                        </p>
                                        <div class="meta-info">
                                            <div class="meta-action">
                                                <button type="button" class="btn btn-block btn-success">
                                                    View Full Recipe
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')
    <script src="{{asset('js/ie11fix/fn.fix-padStart.js')}}"></script>
    <script src="{{asset('plugins/editors/quill/quill.js')}}"></script>
    <script src="{{asset('plugins/sweetalerts/sweetalert2.min.js')}}"></script>
    <script src="{{asset('plugins/notification/snackbar/snackbar.min.js')}}"></script>
    <script src="{{asset('js/apps/custom-mailbox.js')}}"></script>
@endsection
